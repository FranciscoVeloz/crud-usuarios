<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <title>Usuarios</title>
</head>

<body>

    <div class="container mt-5 text-center">
        <h1>
            Usuarios
        </h1>
    </div>

    <div class="container mt-5">
        <div class="row">
            <div class="col-md-10 mx-auto">
                @if($errors->any())
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <p>-{{$error}}</p>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
        <div class="row mb-5">
            <div class="col-md-4 col-12 mx-auto text-center">
                <form action="{{ route('users.store') }}" method="POST">
                    @csrf
                    <h2 class="mb-3">Ingresa usuario</h2>
                    <div class="form-group">
                        <input type="text" class="form-control" name="name" placeholder="Nombre" value="{{old('name')}}">
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control" name="email" placeholder="Email" value="{{old('email')}}">
                    </div>

                    <div class="form-group">
                        <input type="password" class="form-control" name="password" placeholder="Contrasena">
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block">
                            Guardar
                        </button>
                    </div>
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <table class="table">
                    <thead>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Email</th>
                        <th>Acciones</th>
                    </thead>

                    <tbody>
                        @foreach ($users as $u)
                            <tr>
                                <td>{{ $u->id }}</td>
                                <td>{{ $u->name }}</td>
                                <td>{{ $u->email }}</td>
                                <td>
                                    <form action="{{ route('users.destroy', $u->id) }}" method="POST">
                                        @method('DELETE')
                                        @csrf
                                        <input 
                                        type="submit" 
                                        value="Eliminar" 
                                        class="btn btn-danger"
                                        onclick="return confirm('Quieres eliminar este dato?')"
                                        >
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>

</html>
